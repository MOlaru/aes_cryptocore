# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'D:\Licenta_2019\CryptoPython\gui_Main.ui'
#
# Created by: PyQt5 UI code generator 5.12.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(751, 640)
        self.buttonBox = QtWidgets.QDialogButtonBox(Dialog)
        self.buttonBox.setGeometry(QtCore.QRect(490, 20, 81, 71))
        self.buttonBox.setOrientation(QtCore.Qt.Vertical)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.Clients = QtWidgets.QTabWidget(Dialog)
        self.Clients.setGeometry(QtCore.QRect(20, 90, 721, 461))
        self.Clients.setObjectName("Clients")
        self.widget = QtWidgets.QWidget()
        self.widget.setObjectName("widget")
        self.bt_set_pin = QtWidgets.QPushButton(self.widget)
        self.bt_set_pin.setGeometry(QtCore.QRect(610, 10, 93, 28))
        self.bt_set_pin.setObjectName("bt_set_pin")
        self.lineEdit_pin = QtWidgets.QLineEdit(self.widget)
        self.lineEdit_pin.setGeometry(QtCore.QRect(590, 40, 113, 22))
        self.lineEdit_pin.setObjectName("lineEdit_pin")
        self.textEdit_rx = QtWidgets.QTextEdit(self.widget)
        self.textEdit_rx.setGeometry(QtCore.QRect(300, 10, 251, 271))
        self.textEdit_rx.setObjectName("textEdit_rx")
        self.textEdit_tx = QtWidgets.QTextEdit(self.widget)
        self.textEdit_tx.setGeometry(QtCore.QRect(300, 290, 251, 131))
        self.textEdit_tx.setObjectName("textEdit_tx")
        self.bt_send = QtWidgets.QPushButton(self.widget)
        self.bt_send.setGeometry(QtCore.QRect(590, 290, 81, 81))
        self.bt_send.setObjectName("bt_send")
        self.textEdit_rx_d = QtWidgets.QTextEdit(self.widget)
        self.textEdit_rx_d.setEnabled(False)
        self.textEdit_rx_d.setGeometry(QtCore.QRect(10, 10, 251, 271))
        self.textEdit_rx_d.setObjectName("textEdit_rx_d")
        self.textEdit_tx_d = QtWidgets.QTextEdit(self.widget)
        self.textEdit_tx_d.setEnabled(False)
        self.textEdit_tx_d.setGeometry(QtCore.QRect(10, 290, 251, 131))
        self.textEdit_tx_d.setObjectName("textEdit_tx_d")
        self.line = QtWidgets.QFrame(self.widget)
        self.line.setGeometry(QtCore.QRect(273, 0, 21, 431))
        self.line.setFrameShape(QtWidgets.QFrame.VLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line.setObjectName("line")
        self.Clients.addTab(self.widget, "")
        self.tab_2 = QtWidgets.QWidget()
        self.tab_2.setObjectName("tab_2")
        self.Clients.addTab(self.tab_2, "")
        self.bt_add_client = QtWidgets.QPushButton(Dialog)
        self.bt_add_client.setGeometry(QtCore.QRect(10, 20, 101, 28))
        self.bt_add_client.setObjectName("bt_add_client")
        self.bt_clear = QtWidgets.QPushButton(Dialog)
        self.bt_clear.setGeometry(QtCore.QRect(10, 570, 81, 28))
        self.bt_clear.setObjectName("bt_clear")

        self.retranslateUi(Dialog)
        self.Clients.setCurrentIndex(0)
        self.buttonBox.accepted.connect(Dialog.accept)
        self.buttonBox.rejected.connect(Dialog.reject)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Aes Messenger"))
        self.bt_set_pin.setText(_translate("Dialog", "SetPIN"))
        self.bt_send.setText(_translate("Dialog", "Send"))
        self.Clients.setTabText(self.Clients.indexOf(self.widget), _translate("Dialog", "Client1"))
        self.Clients.setTabText(self.Clients.indexOf(self.tab_2), _translate("Dialog", "Tab 2"))
        self.bt_add_client.setText(_translate("Dialog", "Add New Client"))
        self.bt_clear.setText(_translate("Dialog", "Clear"))


