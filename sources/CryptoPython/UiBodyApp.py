from PyQt5 import QtWidgets, uic
from PyQt5.QtWidgets import QMainWindow, QApplication, QPushButton, QWidget, QAction, QTabWidget,QVBoxLayout
from PyQt5 import QtCore
import sys
from SerialListener import SerialListener
import threading
from AesDev import MyEncrypt
import time

default_key = '00000000000000000000000000000000'
qtCreatorFile = "gui_Main.ui"  # Enter file here.
COM = 'COM1'
BAUD_RATE = 115200
ser = SerialListener(COM, BAUD_RATE)
NO_OF_BYTE = 5
PIN_SIZE = 5
AES_SIZE_DECRYPT = 16

aes_core = MyEncrypt(default_key)
list_aes_core = list()
list_aes_core.append(aes_core)

mutex = threading.Lock()

serial_busy = 0
master_access = 0


class AesApp(QMainWindow):

    def __init__(self):
        super().__init__()
        self.ui = uic.loadUi(qtCreatorFile)
        self.ui.Clients.removeTab(1)
        self.max_no_of_clients = 4
        self.ui.bt_add_client.clicked.connect(self.add_new_client)
        self.ui.bt_clear.clicked.connect(self.clear_rx_text_box)
        self.ui.bt_send.clicked.connect(self.encode_tx_message)
        self.ui.bt_set_pin.clicked.connect(self.set_new_password)

        self.active_clients = 1

        self.list_key = list()
        self.list_key.append(default_key)

        self.list_bt_send = list()
        self.list_bt_set_pin = list()
        self.list_lineEdit_pin = list()
        self.list_textEdit_rx = list()
        self.list_textEdit_tx = list()
        self.list_textEdit_rx_d = list()
        self.list_textEdit_tx_d = list()

        self.list_bt_send.append(self.ui.bt_send)
        self.list_bt_set_pin.append(self.ui.bt_set_pin)
        self.list_lineEdit_pin.append(self.ui.lineEdit_pin)
        self.list_textEdit_rx.append(self.ui.textEdit_rx)
        self.list_textEdit_tx.append(self.ui.textEdit_tx)
        self.list_textEdit_rx_d.append(self.ui.textEdit_rx_d)
        self.list_textEdit_tx_d.append(self.ui.textEdit_tx_d)

        self.timer = QtCore.QTimer()
        self.timer.setInterval(1000)
        self.timer.timeout.connect(self.start_serial_listener)
        self.timer.start()
        self.rx_message = ''

    def add_new_client(self):
        # Add New Client
        master_access = 1
        while serial_busy == 1:
            time.sleep(0.25)
        if self.active_clients < self.max_no_of_clients:
            self.active_clients += 1
            self.build_tab()
            self.list_bt_send[-1].clicked.connect(self.encode_tx_message)
        else:
            self.test_attributes_access()
        master_access = 0

    def test_attributes_access(self):
        for i in range(self.ui.Clients.count()):
            self.ui.Clients.setCurrentIndex(i)
            self.list_lineEdit_pin[i].setText(str(i))

    def build_tab(self):
        #set tab object Name
        obj_name = "tab" + str(self.active_clients)
        client_name = "Client " + str(self.active_clients)
        self.ui.new_client = QWidget()
        self.ui.new_client.setObjectName(obj_name)

        bt_set_pin = QtWidgets.QPushButton('SetPIN', self.ui.new_client)
        bt_set_pin.setGeometry(QtCore.QRect(610, 10, 93, 28))
        obj_name = "bt_set_pin"
        bt_set_pin.setObjectName(obj_name)
        bt_set_pin.clicked.connect(self.set_new_password)
        self.list_bt_set_pin.append(bt_set_pin)

        lineEdit_pin = QtWidgets.QLineEdit(self.ui.new_client)
        lineEdit_pin.setGeometry(QtCore.QRect(590, 40, 113, 22))
        obj_name = "lineEdit_pin"
        lineEdit_pin.setObjectName(obj_name)
        self.list_lineEdit_pin.append(lineEdit_pin)

        textEdit_rx = QtWidgets.QTextEdit(self.ui.new_client)
        textEdit_rx.setGeometry(QtCore.QRect(300, 10, 251, 271))
        obj_name = "textEdit_rx"
        textEdit_rx.setObjectName(obj_name)
        self.list_textEdit_rx.append(textEdit_rx)

        textEdit_tx = QtWidgets.QTextEdit(self.ui.new_client)
        textEdit_tx.setGeometry(QtCore.QRect(300, 290, 251, 131))
        obj_name = "textEdit_tx"
        textEdit_tx.setObjectName(obj_name)
        self.list_textEdit_tx.append(textEdit_tx)

        bt_send = QtWidgets.QPushButton('Send', self.ui.new_client)
        bt_send.setGeometry(QtCore.QRect(590, 290, 81, 81))
        obj_name = "bt_send"
        bt_send.setObjectName(obj_name)
        self.list_bt_send.append(bt_send)

        textEdit_rx_d = QtWidgets.QTextEdit(self.ui.new_client)
        textEdit_rx_d.setReadOnly(True)
        textEdit_rx_d.setGeometry(QtCore.QRect(10, 10, 251, 271))
        textEdit_rx_d.setObjectName("textEdit_rx_d")
        self.list_textEdit_rx_d.append(textEdit_rx_d)

        textEdit_tx_d = QtWidgets.QTextEdit(self.ui.new_client)
        textEdit_tx_d.setReadOnly(True)
        textEdit_tx_d.setGeometry(QtCore.QRect(10, 290, 251, 131))
        textEdit_tx_d.setObjectName("textEdit_tx_d")
        self.list_textEdit_tx_d.append(textEdit_tx_d)

        line = QtWidgets.QFrame(self.ui.new_client)
        line.setGeometry(QtCore.QRect(273, 0, 21, 431))
        line.setFrameShape(QtWidgets.QFrame.VLine)
        line.setFrameShadow(QtWidgets.QFrame.Sunken)
        line.setObjectName("line")

        self.ui.Clients.addTab(self.ui.new_client, client_name)
        self.list_key.append(default_key)

        aes_core = MyEncrypt(default_key)
        list_aes_core.append(aes_core)
        # self.thread.start()

    def decode_rx_message(self, to_do_msg):
        msg = ""
        for ch in to_do_msg:
            msg = msg + chr(ch)
        return msg

    def start_serial_listener(self):
        global serial_busy
        while master_access == 1:
            time.sleep(0.5)

        if ser.read_try() is True:
            serial_busy = 1
            serial_concat = ser.message
            for i in range(self.ui.Clients.count()):
                decrypt_msg = list_aes_core[i].decrypt_message(serial_concat)
                to_display = self.decode_rx_message(decrypt_msg)
                print(to_display, end="")
                self.list_textEdit_rx[i].insertPlainText(to_display)
            to_print = self.decode_rx_message(serial_concat)
            for i in range(self.ui.Clients.count()):
                self.list_textEdit_rx_d[i].insertPlainText(to_print)
            serial_busy = 0

    def clear_rx_text_box(self):
        index = self.ui.Clients.currentIndex()
        self.list_textEdit_rx[index].clear()
        self.list_textEdit_rx_d[index].clear()
        self.list_textEdit_tx[index].clear()
        self.list_textEdit_tx_d[index].clear()

    def encode_tx_message(self):
        index = self.ui.Clients.currentIndex()
        text = self.list_textEdit_tx[index].toPlainText()
        # encode function
        cipher = list_aes_core[index].encrypt_message(text)

        # send to Serial
        ser.write(cipher)

        # decode message for printing
        cipher = self.decode_rx_message(cipher)
        self.list_textEdit_tx_d[index].append(cipher)
        self.list_textEdit_tx[index].clear()

    def set_new_password(self):
        index = self.ui.Clients.currentIndex()
        pin_text = self.list_lineEdit_pin[index].text()
        if len(pin_text) != PIN_SIZE:
            self.list_lineEdit_pin[index].setText("Lungime pin incorecta")
        else:
            new_key = pin_text + self.list_key[0][len(pin_text): len(self.list_key[0])]
            self.list_key.pop(index)
            self.list_key.insert(index, new_key)
            list_aes_core[index].set_key(new_key)

    def show(self):
        self.ui.show()


if __name__ == "__main__":
    app = QApplication([])

    win = AesApp()
    win.show()

    sys.exit(app.exec())
