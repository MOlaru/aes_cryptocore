import serial


class SerialListener:

    def __init__(self, com_in, baud_rate_in):
        self.COM = com_in
        self.baud_rate = baud_rate_in
        self.ser = serial.Serial(port=self.COM, baudrate=self.baud_rate)
        self.message = ""
        self.error = 0
        if self.ser.isOpen():

            self.ser.flushInput()  # flush input buffer, discarding all its contents
            self.ser.flushOutput()  # flush output buffer, aborting current output and discard all that is in buffer
        else:
            self.ser.open()
            self.ser.flushInput()  # flush input buffer, discarding all its contents
            self.ser.flushOutput()  # flush output buffer, aborting current output and discard all that is in buffer

    def start_reading(self, nr_byte):
        if self.error == 0:
            self.message = self.ser.read(nr_byte)

    def write(self, msg):
        self.ser.write(msg)

    def read_try(self):
        is_data = False
        if self.ser.inWaiting() >= 16:
            self.message = self.ser.read(self.ser.inWaiting())
            is_data = True
        return is_data


if __name__ == "__main__":
    com = 'COM37'
    baud_rate = 115200
    test = SerialListener(com, baud_rate)
    while True:
        test.start_reading(10)
        print("Message : ", test.message)
