from Crypto.Cipher import AES
import binascii
import time


class MyEncrypt:
    def __init__(self, key):
        self.key = binascii.unhexlify(key)
        self.core = AES.new(self.key, AES.MODE_ECB)
        self.KEY_SIZE = 32

    def encrypt_message(self, plain_text):
        working_text = plain_text.encode('utf-8')
        working_text = self.pad(working_text)
        cipher_text = self.core.encrypt(working_text)
        return cipher_text

    def decrypt_message(self, c):
        text = self.core.decrypt(c)
        return text

    def set_key(self, new_key):
        if len(new_key) == self.KEY_SIZE:
            self.key = binascii.unhexlify(new_key)
            self.core = AES.new(self.key, AES.MODE_ECB)

    def pad(self, s):
        return s + b'\0' * (AES.block_size - len(s) % AES.block_size)


if __name__ == "__main__":
    text = "Mama Mama Mama Mama Mama safsadfasdfsafafa"
    # text = text.encode('utf-8')
    KEY = '00000000000000000000000000000000'  # hex string
    core = MyEncrypt(KEY)
    plaintext = binascii.unhexlify('000000000000000000000A')
    start = time.time()
    for i in range(0, 781250):
        cipher = core.encrypt_message(text)
    end = time.time()
    print(end - start)
    # print(binascii.hexlify(cipher).decode('utf-8'))
    # text = core.decrypt_message(cipher)
    # text = str(text, 'utf-8')
    # print(binascii.b2a_uu(text))
    # print(binascii.hexlify(text).decode('utf-8'))
