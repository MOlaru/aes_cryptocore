/*
 * aes_crypto_drv.h
 *
 *  Created on: 13 iun. 2019
 *      Author: Mihai
 */

#ifndef SRC_AES_CRYPTO_DRV_H_
#define SRC_AES_CRYPTO_DRV_H_

/******************************************************************************/
/*                                                                            */
/* aes_crypto_drv.h -- Driver definitions for the Aes Crypto                  */
/*                                                                            */
/******************************************************************************/
/* Author: Mihai Olaru                                                        */
/*                                                                            */
/******************************************************************************/
/* File Description:                                                          */
/*                                                                            */
/* This library contains function implementation for AesCrypto Core module.   */
/*                                                                            */
/*                                                                            */
/******************************************************************************/

/******************************************************************************/


/****************************** Include Files ***************************/

#include "xil_io.h"
#include "xstatus.h"
#include "xil_types.h"

/**************************** Type Definitions **************************/
#define AES_DATA_SIZE_U8			16
#define AES_KEY_SIZE_U8				16
#define AES_MAX_BUFF                1024

typedef struct AesCore {

	u32 CoreAddress;
	u32 ControlReg;
	u32 StatusReg;
	u8  data_in[16];
	u8  key[16];
	u8  data_out[16];

} AesCore;

#define AESCRYPTOCORE_S00_AXI_SLV_REG0_OFFSET 0
#define AESCRYPTOCORE_S00_AXI_SLV_REG1_OFFSET 4
#define AESCRYPTOCORE_S00_AXI_SLV_REG2_OFFSET 8
#define AESCRYPTOCORE_S00_AXI_SLV_REG3_OFFSET 12
#define AESCRYPTOCORE_S00_AXI_SLV_REG4_OFFSET 16
#define AESCRYPTOCORE_S00_AXI_SLV_REG5_OFFSET 20
#define AESCRYPTOCORE_S00_AXI_SLV_REG6_OFFSET 24
#define AESCRYPTOCORE_S00_AXI_SLV_REG7_OFFSET 28
#define AESCRYPTOCORE_S00_AXI_SLV_REG8_OFFSET 32
#define AESCRYPTOCORE_S00_AXI_SLV_REG9_OFFSET 36
#define AESCRYPTOCORE_S00_AXI_SLV_REG10_OFFSET 40
#define AESCRYPTOCORE_S00_AXI_SLV_REG11_OFFSET 44
#define AESCRYPTOCORE_S00_AXI_SLV_REG12_OFFSET 48
#define AESCRYPTOCORE_S00_AXI_SLV_REG13_OFFSET 52

#define AES_ENCRYPT 				1
#define AES_DECRYPT 				0

#define AES_OFFSET_DATA_IN			8
#define AES_OFFSET_KEY				24
#define AES_OFFSET_DATA_OUT			40
#define AES_OFFSET_CTRL				0
#define AES_OFFSET_STS				4

#define AES_CTRL_REGISTER_STATE     0
#define AES_CTRL_REGISTER_START		1


#define READ_WRITE_MUL_FACTOR 0x10

#define AESCRYPTOCORE_mReadReg(BaseAddress, RegOffset) \
    Xil_In32((BaseAddress) + (RegOffset))


#define AES_BASEADDR0 				XPAR_AESCRYPTOCORE_0_S00_AXI_BASEADDR

#define AESCRYPTOCORE_mWriteReg(BaseAddress, RegOffset, Data) \
  	Xil_Out32((BaseAddress) + (RegOffset), (u32)(Data))

/************************** Extern Definitions ************************/

extern u8 encrypted_buff[AES_MAX_BUFF];
extern u8 decrypted_buff[AES_MAX_BUFF];

/************************** Function Definitions ************************/

void AES_begin(AesCore* InstancePtr, u32 CORE_Address);
void AES_setEncryptionState(AesCore* InstancePtr, u8 encryption_state);
u8 	 AES_getEncryptionState(AesCore* InstancePtr);
void AES_setData(AesCore*InstancePtr, u8*data);
void AES_setKey(AesCore*InstancePtr, u8*key);
void AES_setStart(AesCore*InstancePtr);
void AES_setStop(AesCore*InstancePtr);

void AES_DriveBuff(u32 StartAddress, u8* buff);
void AES_StoreBuff(u32 StartAddress, u8*buff);
void AES_SendRequestBlocking(AesCore*InstancePtr);

void AES_EncryptString(AesCore* InstancePtr, u8* s);
void AES_DecryptString(AesCore* InstancePtr, u8* s);

void AES_CopyBuff(u8*ToBuff, u8*FromBuff, u32 FromLen);

XStatus AESCRYPTOCORE_Reg_SelfTest(void * baseaddr_p);
#endif /* SRC_AES_CRYPTO_DRV_H_ */
