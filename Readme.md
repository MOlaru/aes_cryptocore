# AES CryptoCore

An IP used for real time encryption

## Repository structure

 * doc: ppt presentation about the project and other useful informations
 * ip_proj: Vivado project with the AESCryptoCore IP
 * verif_proj: Vivado project with a verification environment for the AESCryptoCore
 * soc_proj: Integration of the AESCryptoCore IP with the Zynq Processor via AXI bus.
 * sources:
    * CryptoPython: python app for UART encryption
    * HDL: all the hardware sources
    * ZynqApp: a demo app where the AESCryptoCore IP is used for secure UART communication