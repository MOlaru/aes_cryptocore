onbreak {quit -f}
onerror {quit -f}

vsim -t 1ps -lib xil_defaultlib AesCrypto_opt

do {wave.do}

view wave
view structure
view signals

do {AesCrypto.udo}

run -all

quit -force
