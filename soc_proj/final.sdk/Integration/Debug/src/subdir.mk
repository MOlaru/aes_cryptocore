################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
LD_SRCS += \
../src/lscript.ld 

C_SRCS += \
../src/GPIO_Leds.c \
../src/Integration.c \
../src/aes_crypto_drv.c \
../src/fpga_commands.c \
../src/platform.c \
../src/uart_communication.c 

OBJS += \
./src/GPIO_Leds.o \
./src/Integration.o \
./src/aes_crypto_drv.o \
./src/fpga_commands.o \
./src/platform.o \
./src/uart_communication.o 

C_DEPS += \
./src/GPIO_Leds.d \
./src/Integration.d \
./src/aes_crypto_drv.d \
./src/fpga_commands.d \
./src/platform.d \
./src/uart_communication.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: ARM v7 gcc compiler'
	arm-none-eabi-gcc -Wall -O0 -g3 -c -fmessage-length=0 -MT"$@" -mcpu=cortex-a9 -mfpu=vfpv3 -mfloat-abi=hard -I../../Integration_bsp/ps7_cortexa9_0/include -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


