/*
 * GPIO_Leds.h
 *
 *  Created on: 22 iun. 2019
 *      Author: Mihai
 */

#ifndef SRC_GPIO_LEDS_H_
#define SRC_GPIO_LEDS_H_

#include "xparameters.h"
#include "xgpio.h"
#include "xplatform_info.h"
#include "xstatus.h"

#define LED_ON					1
#define LED_OFF					0

#define LED_0					0
#define LED_1					1
#define LED_2					2
#define LED_3					3

#define NR_LED 					4

#define LEDS_GPIO_ID 			XPAR_GPIO_1_DEVICE_ID
#define LED_CONFIG()			LedSetUp(LEDS_GPIO_ID, &led_gpio);

extern XGpio led_gpio;
extern u8 led_value;

int LedSetUp(u16 DeviceId, XGpio* gpio);
void LedWrire(u8 nr_leg, u8 val);

#endif /* SRC_GPIO_LEDS_H_ */
