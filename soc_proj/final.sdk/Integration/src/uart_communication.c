/*
 * uart_communication.c
 *
 *  Created on: 22 iun. 2019
 *      Author: Mihai
 */
#include "uart_communication.h"

XUartPs Uart_Ps;

int UartSetUp(u16 DeviceId)
{
	int Status;
	XUartPs_Config *Config;

	Config = XUartPs_LookupConfig(DeviceId);
	if (NULL == Config) {
		return XST_FAILURE;
	}

	Status = XUartPs_CfgInitialize(&Uart_Ps, Config, Config->BaseAddress);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}
	else{
		XUartPs_SetBaudRate(&Uart_Ps, 115200);
	}
	return XST_SUCCESS;
}
