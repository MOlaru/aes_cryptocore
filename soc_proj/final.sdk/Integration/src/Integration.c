/******************************************************************************
*
* Copyright (C) 2009 - 2014 Xilinx, Inc.  All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* Use of the Software is limited solely to applications:
* (a) running on a Xilinx device, or
* (b) that interact with a Xilinx device through a bus or interconnect.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
* XILINX  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
* OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*
* Except as contained in this notice, the name of the Xilinx shall not be used
* in advertising or otherwise to promote the sale, use or other dealings in
* this Software without prior written authorization from Xilinx.
*
******************************************************************************/

/*
 * helloworld.c: simple test application
 *
 * This application configures UART 16550 to baud rate 9600.
 * PS7 UART (Zynq) is not initialized by this application, since
 * bootrom/bsp configures it to baud rate 115200
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   uartns550   9600
 *   uartlite    Configurable only in HW design
 *   ps7_uart    115200 (configured by bootrom/bsp)
 */

#include <stdio.h>
#include "platform.h"
#include "xil_printf.h"
#include "aes_crypto_drv.h"
#include "GPIO_Leds.h"
#include "uart_communication.h"
#include "fpga_commands.h"
#include "xtime_l.h"

void execute_commands();
void printfBlock(u8* buff);

u8 rx_buff[AES_DATA_SIZE_U8];
AesCore test;

void printfBlock(u8* buff)
{
	// print 128 bits in hex
	int i;
	xil_printf("Buff Print\n\r");
	for (i = 0; i < AES_DATA_SIZE_U8; i++)
	{
		xil_printf("%02x",buff[i]);
	}
	xil_printf("\n\r");
}

void recording_commands()
{
	u32 ReceivedCount = 0;
	for(;;)
	{
		ReceivedCount = 0;
		// waiting for 128 bits for decryption

		while (ReceivedCount < AES_DATA_SIZE_U8) {
			ReceivedCount += XUartPs_Recv(&Uart_Ps, rx_buff+ReceivedCount,
			(AES_DATA_SIZE_U8 - ReceivedCount));
		}

		//decrypt the message and start to look for commands
		// result after decryption is in decrypted_buff + i
		AES_DecryptString(&test, rx_buff);
		for(int i = 0; i < AES_DATA_SIZE_U8; i++)
		{
			if (decrypted_buff[i] == START_CHAR)
			{

				extracts_commands(&current_command, decrypted_buff+i);
				execute_commands();
			}
		}

	}
}

void led_control(u8 value)
{
	u8 state = value & 1;
	u8 led_nr = (value & 0x6) >> 1;
	LedWrire(led_nr, state);
	cmd_led_msg[LED_MSG_ID] = led_nr + '0';
	cmd_led_msg[LED_MSG_STATE] = state + '0';
	AES_EncryptString(&test, cmd_led_msg);
	xil_printf("%s", encrypted_buff);
}

void execute_commands()
{
	//AES_EncryptString(&test, rx_buff);
	switch(current_command.opcode)
	{
		case Invalid:

			AES_EncryptString(&test, cmd_invalid_msg);
			xil_printf("%s", encrypted_buff);
			break;
		case Led:
			led_control(current_command.value);
			break;
		case State:
			AES_EncryptString(&test, cmd_state_msg);
			xil_printf("%s", encrypted_buff);
			break;
		default:
			xil_printf("Opcode is unknown!\r\n");
			break;
	}
	//xil_printf("CMD=%s\r\n", rx_buff);
}

void printMemory(u32 base_addr, int no_of_words)
{
	// Print Axi memory, It's used for Debugging AES Module
	int i;
	u32 data;
	xil_printf("Memory Print\n\r");
	for (i = 0; i < no_of_words;i++)
	{
		data = AESCRYPTOCORE_mReadReg (base_addr, i*4);
		xil_printf("%08x\n\r", data);
	}
	xil_printf("End of Memory\n\r");
}

int main()
{
	u8 en_buff[30] = "Hello from the other side!\r\n";
	AesCore Debug = {0};
	XTime start, stop, rez;
	int i;
	init_platform();
	LED_CONFIG();
	UartSetUp(UART_DEVICE_ID);
	LedWrire(LED_1, LED_ON);

    AES_begin(&test, AES_BASEADDR0);
    AES_begin(&Debug, AES_BASEADDR0);

    //AES_EncryptString(&test, en_buff);
    AES_EncryptString(&Debug, en_buff);


    xil_printf("%s", encrypted_buff);

    //XUartPs_Send(&Uart_Ps, encrypted_buff, 32);

    recording_commands();
//    XTime_GetTime(&start);
//    for (i = 0; i < 781250; i++)
//    {
//    	AES_SendRequestBlocking(&Debug);
//    }
//    XTime_GetTime(&stop);
//    rez = stop - start;
//    xil_printf("Time1 = %llu\r\n", rez);
	cleanup_platform();
    return 0;
}
