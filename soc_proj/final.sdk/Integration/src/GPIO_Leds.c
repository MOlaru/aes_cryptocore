/*
 * GPIO_Leds.c
 *
 *  Created on: 22 iun. 2019
 *      Author: Mihai
 */
#include "GPIO_Leds.h"

XGpio led_gpio;
u8 led_value;


int LedSetUp(u16 DeviceId, XGpio* gpio)
{
	int Status = 0;

	// initialize the GPIO driver.
	XGpio_Initialize(gpio, XPAR_AXI_GPIO_1_DEVICE_ID);
	// configure the LED pin as output
	// status checks skipped for readability
	XGpio_SetDataDirection(gpio, 1, 0x0);
	XGpio_DiscreteWrite(gpio, 1, 0);
	led_value = 0;

	return Status;
}


void LedWrire(u8 nr_leg, u8 val)
{
	if (nr_leg < NR_LED)  {
		if(val == 1){
			led_value |= (u8) (1 << nr_leg);
		}
		else {
			led_value &= (u8) (~(1 << nr_leg));
		}
		XGpio_DiscreteWrite(&led_gpio, 1, led_value);
	}
}
