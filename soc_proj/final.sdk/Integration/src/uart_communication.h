/*
 * uart_communication.h
 *
 *  Created on: 22 iun. 2019
 *      Author: Mihai
 */

#ifndef SRC_UART_COMMUNICATION_H_
#define SRC_UART_COMMUNICATION_H_
#include "xparameters.h"
#include "xuartps.h"
#include "xil_printf.h"

extern XUartPs Uart_Ps;

int UartSetUp(u16 DeviceId);

#endif /* SRC_UART_COMMUNICATION_H_ */
