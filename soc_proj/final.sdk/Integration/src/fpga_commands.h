/*
 * fpga_commands.h
 *
 *  Created on: 22 iun. 2019
 *      Author: Mihai
 */

#ifndef SRC_FPGA_COMMANDS_H_
#define SRC_FPGA_COMMANDS_H_

#include "xil_types.h"

#define START_CHAR 			'<'
#define END_CHAR   			'>'
#define COMMAND_LENGTH		2


#define LED_MSG_ID 						4
#define LED_MSG_STATE 					9
typedef enum {Invalid, Led, State} Opcode_e;

typedef struct Command {

	Opcode_e opcode;
	u8 value;

} Command;

extern Command current_command;
extern u8 cmd_invalid_msg[18];
extern u8 cmd_state_msg[23];
extern u8 cmd_led_msg[13];

void extracts_commands(Command * com, u8* buffer);

#endif /* SRC_FPGA_COMMANDS_H_ */
