/*
 * fpga_commands.c
 *
 *  Created on: 22 iun. 2019
 *      Author: Mihai
 */
#include "fpga_commands.h"

Command current_command;

u8 cmd_invalid_msg[18] = "Invalid Command\r\n";
u8 cmd_state_msg[23] = "Everything is GOOD!!\r\n";
u8 cmd_led_msg[13] = "Led   is  \r\n";


Opcode_e get_opcode(u8 value)
{
	Opcode_e ret_val= Invalid;
	switch(value)
	{
		case 'L': {
			ret_val = Led;
			break;
		}
		case 'S': {
			ret_val = State;
			break;
		}

	}
	return ret_val;
}

void extracts_commands(Command* com, u8* buffer)
{
	u32 index = 0;
	u8 cmd_is_start = 0, cmd_is_end = 0;
	while (buffer[index] != '\0')
	{
		if(buffer[index] == START_CHAR)
		{
			cmd_is_start = index;
		}
		else {
			if (buffer[index] == END_CHAR)
			{
				cmd_is_end = index;
				break;
			}
		}
		index++;
	}
	if (cmd_is_end - cmd_is_start == COMMAND_LENGTH + 1)
	{
		com->opcode = get_opcode(buffer[cmd_is_start + 1]);
		com->value = buffer[cmd_is_start + 2] - '0';
	}
	else {
		com->opcode = Invalid;
	}
}
