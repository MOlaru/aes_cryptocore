
�
I%sTime (s): cpu = %s ; elapsed = %s . Memory (MB): peak = %s ; gain = %s
268*common2$
create_project: 2default:default2
00:00:032default:default2
00:00:062default:default2
317.6482default:default2
86.8592default:defaultZ17-268h px� 

Command: %s
53*	vivadotcl2N
:synth_design -top AesCryptoCore_v1_0 -part xc7z020clg400-12default:defaultZ4-113h px� 
:
Starting synth_design
149*	vivadotclZ4-321h px� 
�
@Attempting to get a license for feature '%s' and/or device '%s'
308*common2
	Synthesis2default:default2
xc7z0202default:defaultZ17-347h px� 
�
0Got license for feature '%s' and/or device '%s'
310*common2
	Synthesis2default:default2
xc7z0202default:defaultZ17-349h px� 
�
%s*synth2�
rStarting Synthesize : Time (s): cpu = 00:00:06 ; elapsed = 00:00:09 . Memory (MB): peak = 425.840 ; gain = 95.883
2default:defaulth px� 
�
synthesizing module '%s'%s4497*oasys2&
AesCryptoCore_v1_02default:default2
 2default:default2Y
Cd:/digiLent_test/ip_repo/AesCryptoCore_1.0/hdl/AesCryptoCore_v1_0.v2default:default2
42default:default8@Z8-6157h px� 
j
%s
*synth2R
>	Parameter C_S00_AXI_DATA_WIDTH bound to: 32 - type: integer 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter C_S00_AXI_ADDR_WIDTH bound to: 6 - type: integer 
2default:defaulth p
x
� 
�
synthesizing module '%s'%s4497*oasys2.
AesCryptoCore_v1_0_S00_AXI2default:default2
 2default:default2a
Kd:/digiLent_test/ip_repo/AesCryptoCore_1.0/hdl/AesCryptoCore_v1_0_S00_AXI.v2default:default2
252default:default8@Z8-6157h px� 
h
%s
*synth2P
<	Parameter C_S_AXI_DATA_WIDTH bound to: 32 - type: integer 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter C_S_AXI_ADDR_WIDTH bound to: 6 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter ADDR_LSB bound to: 2 - type: integer 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter OPT_MEM_ADDR_BITS bound to: 3 - type: integer 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter NO_OF_REGISTERS bound to: 14 - type: integer 
2default:defaulth p
x
� 
�
synthesizing module '%s'%s4497*oasys2
aes_top2default:default2
 2default:default2N
8d:/digiLent_test/ip_repo/AesCryptoCore_1.0/src/aes_top.v2default:default2
412default:default8@Z8-6157h px� 
S
%s
*synth2;
'	Parameter state_idle bound to: 2'b00 
2default:defaulth p
x
� 
S
%s
*synth2;
'	Parameter state_work bound to: 2'b01 
2default:defaulth p
x
� 
U
%s
*synth2=
)	Parameter state_finish bound to: 2'b10 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter state_key_gen_idle bound to: 2'b00 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter state_key_gen_compare bound to: 2'b01 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter state_key_gen_wait bound to: 2'b10 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter state_key_gen_good bound to: 2'b11 
2default:defaulth p
x
� 
�
-case statement is not full and has no default155*oasys2N
8d:/digiLent_test/ip_repo/AesCryptoCore_1.0/src/aes_top.v2default:default2
1632default:default8@Z8-155h px� 
�
synthesizing module '%s'%s4497*oasys2 
aes_s_box1282default:default2
 2default:default2R
<d:/digiLent_test/ip_repo/AesCryptoCore_1.0/src/aes_sbox128.v2default:default2
152default:default8@Z8-6157h px� 
�
synthesizing module '%s'%s4497*oasys2
bSbox2default:default2
 2default:default2P
:d:/digiLent_test/ip_repo/AesCryptoCore_1.0/src/aes_s_box.v2default:default2
212default:default8@Z8-6157h px� 
�
synthesizing module '%s'%s4497*oasys2 
select_not_82default:default2
 2default:default2S
=d:/digiLent_test/ip_repo/AesCryptoCore_1.0/src/select_not_8.v2default:default2
212default:default8@Z8-6157h px� 
�
synthesizing module '%s'%s4497*oasys2
mux2_12default:default2
 2default:default2M
7d:/digiLent_test/ip_repo/AesCryptoCore_1.0/src/mux2_1.v2default:default2
202default:default8@Z8-6157h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2
mux2_12default:default2
 2default:default2
12default:default2
12default:default2M
7d:/digiLent_test/ip_repo/AesCryptoCore_1.0/src/mux2_1.v2default:default2
202default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2 
select_not_82default:default2
 2default:default2
22default:default2
12default:default2S
=d:/digiLent_test/ip_repo/AesCryptoCore_1.0/src/select_not_8.v2default:default2
212default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys2
gf_inv_82default:default2
 2default:default2O
9d:/digiLent_test/ip_repo/AesCryptoCore_1.0/src/gf_inv_8.v2default:default2
222default:default8@Z8-6157h px� 
�
synthesizing module '%s'%s4497*oasys2
gf_inv_42default:default2
 2default:default2O
9d:/digiLent_test/ip_repo/AesCryptoCore_1.0/src/gf_inv_4.v2default:default2
222default:default8@Z8-6157h px� 
�
synthesizing module '%s'%s4497*oasys2
gf_inv_22default:default2
 2default:default2O
9d:/digiLent_test/ip_repo/AesCryptoCore_1.0/src/gf_inv_2.v2default:default2
222default:default8@Z8-6157h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2
gf_inv_22default:default2
 2default:default2
32default:default2
12default:default2O
9d:/digiLent_test/ip_repo/AesCryptoCore_1.0/src/gf_inv_2.v2default:default2
222default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys2
gf_mul_22default:default2
 2default:default2O
9d:/digiLent_test/ip_repo/AesCryptoCore_1.0/src/gf_mul_2.v2default:default2
222default:default8@Z8-6157h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2
gf_mul_22default:default2
 2default:default2
42default:default2
12default:default2O
9d:/digiLent_test/ip_repo/AesCryptoCore_1.0/src/gf_mul_2.v2default:default2
222default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2
gf_inv_42default:default2
 2default:default2
52default:default2
12default:default2O
9d:/digiLent_test/ip_repo/AesCryptoCore_1.0/src/gf_inv_4.v2default:default2
222default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys2
gf_mul_42default:default2
 2default:default2O
9d:/digiLent_test/ip_repo/AesCryptoCore_1.0/src/gf_mul_4.v2default:default2
222default:default8@Z8-6157h px� 
�
synthesizing module '%s'%s4497*oasys2 
gf_mul_scl_22default:default2
 2default:default2S
=d:/digiLent_test/ip_repo/AesCryptoCore_1.0/src/gf_mul_scl_2.v2default:default2
222default:default8@Z8-6157h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2 
gf_mul_scl_22default:default2
 2default:default2
62default:default2
12default:default2S
=d:/digiLent_test/ip_repo/AesCryptoCore_1.0/src/gf_mul_scl_2.v2default:default2
222default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2
gf_mul_42default:default2
 2default:default2
72default:default2
12default:default2O
9d:/digiLent_test/ip_repo/AesCryptoCore_1.0/src/gf_mul_4.v2default:default2
222default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2
gf_inv_82default:default2
 2default:default2
82default:default2
12default:default2O
9d:/digiLent_test/ip_repo/AesCryptoCore_1.0/src/gf_inv_8.v2default:default2
222default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2
bSbox2default:default2
 2default:default2
92default:default2
12default:default2P
:d:/digiLent_test/ip_repo/AesCryptoCore_1.0/src/aes_s_box.v2default:default2
212default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2 
aes_s_box1282default:default2
 2default:default2
102default:default2
12default:default2R
<d:/digiLent_test/ip_repo/AesCryptoCore_1.0/src/aes_sbox128.v2default:default2
152default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys2"
aes_shift_rows2default:default2
 2default:default2U
?d:/digiLent_test/ip_repo/AesCryptoCore_1.0/src/aes_shift_rows.v2default:default2
152default:default8@Z8-6157h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2"
aes_shift_rows2default:default2
 2default:default2
112default:default2
12default:default2U
?d:/digiLent_test/ip_repo/AesCryptoCore_1.0/src/aes_shift_rows.v2default:default2
152default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys2 
key_schedule2default:default2
 2default:default2W
Ad:/digiLent_test/ip_repo/AesCryptoCore_1.0/src/aes_key_schedule.v2default:default2
192default:default8@Z8-6157h px� 
�
synthesizing module '%s'%s4497*oasys2

function_g2default:default2
 2default:default2U
?d:/digiLent_test/ip_repo/AesCryptoCore_1.0/src/aes_function_g.v2default:default2
192default:default8@Z8-6157h px� 
�
synthesizing module '%s'%s4497*oasys2#
round_constants2default:default2
 2default:default2Z
Dd:/digiLent_test/ip_repo/AesCryptoCore_1.0/src/aes_round_constants.v2default:default2
12default:default8@Z8-6157h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2#
round_constants2default:default2
 2default:default2
122default:default2
12default:default2Z
Dd:/digiLent_test/ip_repo/AesCryptoCore_1.0/src/aes_round_constants.v2default:default2
12default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2

function_g2default:default2
 2default:default2
132default:default2
12default:default2U
?d:/digiLent_test/ip_repo/AesCryptoCore_1.0/src/aes_function_g.v2default:default2
192default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2 
key_schedule2default:default2
 2default:default2
142default:default2
12default:default2W
Ad:/digiLent_test/ip_repo/AesCryptoCore_1.0/src/aes_key_schedule.v2default:default2
192default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys2

MixColumns2default:default2
 2default:default2V
@d:/digiLent_test/ip_repo/AesCryptoCore_1.0/src/aes_mix_columns.v2default:default2
172default:default8@Z8-6157h px� 
�
synthesizing module '%s'%s4497*oasys2
x_times2default:default2
 2default:default2N
8d:/digiLent_test/ip_repo/AesCryptoCore_1.0/src/x_times.v2default:default2
222default:default8@Z8-6157h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2
x_times2default:default2
 2default:default2
152default:default2
12default:default2N
8d:/digiLent_test/ip_repo/AesCryptoCore_1.0/src/x_times.v2default:default2
222default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2

MixColumns2default:default2
 2default:default2
162default:default2
12default:default2V
@d:/digiLent_test/ip_repo/AesCryptoCore_1.0/src/aes_mix_columns.v2default:default2
172default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2
aes_top2default:default2
 2default:default2
172default:default2
12default:default2N
8d:/digiLent_test/ip_repo/AesCryptoCore_1.0/src/aes_top.v2default:default2
412default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2.
AesCryptoCore_v1_0_S00_AXI2default:default2
 2default:default2
182default:default2
12default:default2a
Kd:/digiLent_test/ip_repo/AesCryptoCore_1.0/hdl/AesCryptoCore_v1_0_S00_AXI.v2default:default2
252default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2&
AesCryptoCore_v1_02default:default2
 2default:default2
192default:default2
12default:default2Y
Cd:/digiLent_test/ip_repo/AesCryptoCore_1.0/hdl/AesCryptoCore_v1_0.v2default:default2
42default:default8@Z8-6155h px� 
�
!design %s has unconnected port %s3331*oasys2 
key_schedule2default:default2
encrypt2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2(
debug_interface[127]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2(
debug_interface[126]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2(
debug_interface[125]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2(
debug_interface[124]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2(
debug_interface[123]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2(
debug_interface[122]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2(
debug_interface[121]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2(
debug_interface[120]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2(
debug_interface[119]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2(
debug_interface[118]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2(
debug_interface[117]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2(
debug_interface[116]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2(
debug_interface[115]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2(
debug_interface[114]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2(
debug_interface[113]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2(
debug_interface[112]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2(
debug_interface[111]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2(
debug_interface[110]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2(
debug_interface[109]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2(
debug_interface[108]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2(
debug_interface[107]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2(
debug_interface[106]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2(
debug_interface[105]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2(
debug_interface[104]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2(
debug_interface[103]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2(
debug_interface[102]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2(
debug_interface[101]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2(
debug_interface[100]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2'
debug_interface[99]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2'
debug_interface[98]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2'
debug_interface[97]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2'
debug_interface[96]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2'
debug_interface[95]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2'
debug_interface[94]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2'
debug_interface[93]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2'
debug_interface[92]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2'
debug_interface[91]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2'
debug_interface[90]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2'
debug_interface[89]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2'
debug_interface[88]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2'
debug_interface[87]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2'
debug_interface[86]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2'
debug_interface[85]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2'
debug_interface[84]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2'
debug_interface[83]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2'
debug_interface[82]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2'
debug_interface[81]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2'
debug_interface[80]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2'
debug_interface[79]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2'
debug_interface[78]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2'
debug_interface[77]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2'
debug_interface[76]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2'
debug_interface[75]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2'
debug_interface[74]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2'
debug_interface[73]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2'
debug_interface[72]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2'
debug_interface[71]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2'
debug_interface[70]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2'
debug_interface[69]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2'
debug_interface[68]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2'
debug_interface[67]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2'
debug_interface[66]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2'
debug_interface[65]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2'
debug_interface[64]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2'
debug_interface[63]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2'
debug_interface[62]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2'
debug_interface[61]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2'
debug_interface[60]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2'
debug_interface[59]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2'
debug_interface[58]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2'
debug_interface[57]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2'
debug_interface[56]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2'
debug_interface[55]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2'
debug_interface[54]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2'
debug_interface[53]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2'
debug_interface[52]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2'
debug_interface[51]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2'
debug_interface[50]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2'
debug_interface[49]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2'
debug_interface[48]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2'
debug_interface[47]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2'
debug_interface[46]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2'
debug_interface[45]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2'
debug_interface[44]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2'
debug_interface[43]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2'
debug_interface[42]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2'
debug_interface[41]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2'
debug_interface[40]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2'
debug_interface[39]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2'
debug_interface[38]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2'
debug_interface[37]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2'
debug_interface[36]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2'
debug_interface[35]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2'
debug_interface[34]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2'
debug_interface[33]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2'
debug_interface[32]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2'
debug_interface[31]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2'
debug_interface[30]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2
aes_top2default:default2'
debug_interface[29]2default:defaultZ8-3331h px� 
�
�Message '%s' appears more than %s times and has been disabled. User can change this message limit to see more message instances.
14*common2 
Synth 8-33312default:default2
1002default:defaultZ17-14h px� 
�
%s*synth2�
sFinished Synthesize : Time (s): cpu = 00:00:09 ; elapsed = 00:00:12 . Memory (MB): peak = 481.547 ; gain = 151.590
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
~Finished Constraint Validation : Time (s): cpu = 00:00:09 ; elapsed = 00:00:12 . Memory (MB): peak = 481.547 ; gain = 151.590
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
V
%s
*synth2>
*Start Loading Part and Timing Information
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
J
%s
*synth22
Loading part: xc7z020clg400-1
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Loading Part and Timing Information : Time (s): cpu = 00:00:09 ; elapsed = 00:00:12 . Memory (MB): peak = 481.547 ; gain = 151.590
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
V
Loading part %s157*device2#
xc7z020clg400-12default:defaultZ21-403h px� 
�
3inferred FSM for state register '%s' in module '%s'802*oasys2&
internal_state_reg2default:default2
aes_top2default:defaultZ8-802h px� 
�
[ROM "%s" won't be mapped to Block RAM because address size (%s) smaller than threshold (%s)3996*oasys2

r_cnt_zero2default:default2
42default:default2
52default:defaultZ8-5544h px� 
�
^ROM "%s" won't be mapped to RAM because address size (%s) is larger than maximum supported(%s)3997*oasys2)
w_extended_key_reg[1]2default:default2
322default:default2
252default:defaultZ8-5545h px� 
�
^ROM "%s" won't be mapped to RAM because address size (%s) is larger than maximum supported(%s)3997*oasys2)
w_extended_key_reg[2]2default:default2
322default:default2
252default:defaultZ8-5545h px� 
�
^ROM "%s" won't be mapped to RAM because address size (%s) is larger than maximum supported(%s)3997*oasys2)
w_extended_key_reg[3]2default:default2
322default:default2
252default:defaultZ8-5545h px� 
�
^ROM "%s" won't be mapped to RAM because address size (%s) is larger than maximum supported(%s)3997*oasys2)
w_extended_key_reg[4]2default:default2
322default:default2
252default:defaultZ8-5545h px� 
�
^ROM "%s" won't be mapped to RAM because address size (%s) is larger than maximum supported(%s)3997*oasys2)
w_extended_key_reg[5]2default:default2
322default:default2
252default:defaultZ8-5545h px� 
�
^ROM "%s" won't be mapped to RAM because address size (%s) is larger than maximum supported(%s)3997*oasys2)
w_extended_key_reg[6]2default:default2
322default:default2
252default:defaultZ8-5545h px� 
�
^ROM "%s" won't be mapped to RAM because address size (%s) is larger than maximum supported(%s)3997*oasys2)
w_extended_key_reg[7]2default:default2
322default:default2
252default:defaultZ8-5545h px� 
�
^ROM "%s" won't be mapped to RAM because address size (%s) is larger than maximum supported(%s)3997*oasys2)
w_extended_key_reg[8]2default:default2
322default:default2
252default:defaultZ8-5545h px� 
�
^ROM "%s" won't be mapped to RAM because address size (%s) is larger than maximum supported(%s)3997*oasys2)
w_extended_key_reg[9]2default:default2
322default:default2
252default:defaultZ8-5545h px� 
�
^ROM "%s" won't be mapped to RAM because address size (%s) is larger than maximum supported(%s)3997*oasys2*
w_extended_key_reg[10]2default:default2
322default:default2
252default:defaultZ8-5545h px� 
�
^ROM "%s" won't be mapped to RAM because address size (%s) is larger than maximum supported(%s)3997*oasys2*
w_extended_key_reg[11]2default:default2
322default:default2
252default:defaultZ8-5545h px� 
�
[ROM "%s" won't be mapped to Block RAM because address size (%s) smaller than threshold (%s)3996*oasys2!
key_gen_reset2default:default2
42default:default2
52default:defaultZ8-5544h px� 
�
[ROM "%s" won't be mapped to Block RAM because address size (%s) smaller than threshold (%s)3996*oasys2
	round_key2default:default2
22default:default2
52default:defaultZ8-5544h px� 
�
[ROM "%s" won't be mapped to Block RAM because address size (%s) smaller than threshold (%s)3996*oasys2!
round_counter2default:default2
22default:default2
52default:defaultZ8-5544h px� 
�
[ROM "%s" won't be mapped to Block RAM because address size (%s) smaller than threshold (%s)3996*oasys2"
internal_state2default:default2
12default:default2
52default:defaultZ8-5544h px� 
�
[ROM "%s" won't be mapped to Block RAM because address size (%s) smaller than threshold (%s)3996*oasys2$
v2_memory_reg[0]2default:default2
42default:default2
52default:defaultZ8-5544h px� 
�
[ROM "%s" won't be mapped to Block RAM because address size (%s) smaller than threshold (%s)3996*oasys2$
v2_memory_reg[1]2default:default2
42default:default2
52default:defaultZ8-5544h px� 
�
[ROM "%s" won't be mapped to Block RAM because address size (%s) smaller than threshold (%s)3996*oasys2$
v2_memory_reg[2]2default:default2
42default:default2
52default:defaultZ8-5544h px� 
�
[ROM "%s" won't be mapped to Block RAM because address size (%s) smaller than threshold (%s)3996*oasys2$
v2_memory_reg[3]2default:default2
42default:default2
52default:defaultZ8-5544h px� 
�
[ROM "%s" won't be mapped to Block RAM because address size (%s) smaller than threshold (%s)3996*oasys2$
v2_memory_reg[4]2default:default2
42default:default2
52default:defaultZ8-5544h px� 
�
[ROM "%s" won't be mapped to Block RAM because address size (%s) smaller than threshold (%s)3996*oasys2$
v2_memory_reg[5]2default:default2
42default:default2
52default:defaultZ8-5544h px� 
�
[ROM "%s" won't be mapped to Block RAM because address size (%s) smaller than threshold (%s)3996*oasys2$
v2_memory_reg[6]2default:default2
42default:default2
52default:defaultZ8-5544h px� 
�
[ROM "%s" won't be mapped to Block RAM because address size (%s) smaller than threshold (%s)3996*oasys2$
v2_memory_reg[7]2default:default2
42default:default2
52default:defaultZ8-5544h px� 
�
[ROM "%s" won't be mapped to Block RAM because address size (%s) smaller than threshold (%s)3996*oasys2$
v2_memory_reg[8]2default:default2
42default:default2
52default:defaultZ8-5544h px� 
�
[ROM "%s" won't be mapped to Block RAM because address size (%s) smaller than threshold (%s)3996*oasys2$
v2_memory_reg[9]2default:default2
42default:default2
52default:defaultZ8-5544h px� 
�
[ROM "%s" won't be mapped to Block RAM because address size (%s) smaller than threshold (%s)3996*oasys2%
v2_memory_reg[10]2default:default2
42default:default2
52default:defaultZ8-5544h px� 
�
[ROM "%s" won't be mapped to Block RAM because address size (%s) smaller than threshold (%s)3996*oasys2%
v2_memory_reg[11]2default:default2
42default:default2
52default:defaultZ8-5544h px� 
�
[ROM "%s" won't be mapped to Block RAM because address size (%s) smaller than threshold (%s)3996*oasys2%
v2_memory_reg[12]2default:default2
42default:default2
52default:defaultZ8-5544h px� 
�
[ROM "%s" won't be mapped to Block RAM because address size (%s) smaller than threshold (%s)3996*oasys2%
v2_memory_reg[13]2default:default2
42default:default2
52default:defaultZ8-5544h px� 
�
[ROM "%s" won't be mapped to Block RAM because address size (%s) smaller than threshold (%s)3996*oasys2%
v2_memory_reg[14]2default:default2
42default:default2
52default:defaultZ8-5544h px� 
�
%s
*synth2x
d---------------------------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s
*synth2t
`                   State |                     New Encoding |                Previous Encoding 
2default:defaulth p
x
� 
�
%s
*synth2x
d---------------------------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s
*synth2s
_              state_idle |                               00 |                               00
2default:defaulth p
x
� 
�
%s
*synth2s
_              state_work |                               01 |                               01
2default:defaulth p
x
� 
�
%s
*synth2s
_            state_finish |                               10 |                               10
2default:defaulth p
x
� 
�
%s
*synth2s
_                  iSTATE |                               11 |                               11
2default:defaulth p
x
� 
�
%s
*synth2x
d---------------------------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
Gencoded FSM with state register '%s' using encoding '%s' in module '%s'3353*oasys2&
internal_state_reg2default:default2

sequential2default:default2
aes_top2default:defaultZ8-3354h px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished RTL Optimization Phase 2 : Time (s): cpu = 00:00:13 ; elapsed = 00:00:15 . Memory (MB): peak = 509.602 ; gain = 179.645
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
E
%s
*synth2-

Report RTL Partitions: 
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
+| |RTL Partition |Replication |Instances |
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
L
%s
*synth24
 Start RTL Component Statistics 
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      4 Bit       Adders := 1     
2default:defaulth p
x
� 
8
%s
*synth2 
+---XORs : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input    128 Bit         XORs := 5     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     32 Bit         XORs := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      8 Bit         XORs := 53    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   3 Input      8 Bit         XORs := 16    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit         XORs := 140   
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit         XORs := 1428  
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      1 Bit         XORs := 80    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   3 Input      1 Bit         XORs := 20    
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	              128 Bit    Registers := 16    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               32 Bit    Registers := 16    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                6 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                4 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                2 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 8     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input    128 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input    128 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   3 Input    128 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     32 Bit        Muxes := 31    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	  20 Input     32 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      8 Bit        Muxes := 83    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      4 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      4 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   3 Input      2 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 343   
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      1 Bit        Muxes := 15    
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
O
%s
*synth27
#Finished RTL Component Statistics 
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
Y
%s
*synth2A
-Start RTL Hierarchical Component Statistics 
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
O
%s
*synth27
#Hierarchical RTL Component report 
2default:defaulth p
x
� 
;
%s
*synth2#
Module mux2_1 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
=
%s
*synth2%
Module gf_mul_2 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
8
%s
*synth2 
+---XORs : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit         XORs := 2     
2default:defaulth p
x
� 
=
%s
*synth2%
Module gf_inv_4 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
8
%s
*synth2 
+---XORs : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit         XORs := 5     
2default:defaulth p
x
� 
A
%s
*synth2)
Module gf_mul_scl_2 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
8
%s
*synth2 
+---XORs : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit         XORs := 2     
2default:defaulth p
x
� 
=
%s
*synth2%
Module gf_mul_4 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
8
%s
*synth2 
+---XORs : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit         XORs := 2     
2default:defaulth p
x
� 
=
%s
*synth2%
Module gf_inv_8 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
8
%s
*synth2 
+---XORs : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit         XORs := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit         XORs := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      1 Bit         XORs := 4     
2default:defaulth p
x
� 
:
%s
*synth2"
Module bSbox 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
8
%s
*synth2 
+---XORs : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit         XORs := 36    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   3 Input      1 Bit         XORs := 1     
2default:defaulth p
x
� 
C
%s
*synth2+
Module aes_shift_rows 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      8 Bit        Muxes := 8     
2default:defaulth p
x
� 
?
%s
*synth2'
Module function_g 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
8
%s
*synth2 
+---XORs : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      8 Bit         XORs := 1     
2default:defaulth p
x
� 
A
%s
*synth2)
Module key_schedule 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
8
%s
*synth2 
+---XORs : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     32 Bit         XORs := 4     
2default:defaulth p
x
� 
<
%s
*synth2$
Module x_times 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
8
%s
*synth2 
+---XORs : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit         XORs := 3     
2default:defaulth p
x
� 
?
%s
*synth2'
Module MixColumns 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
8
%s
*synth2 
+---XORs : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      8 Bit         XORs := 13    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   3 Input      8 Bit         XORs := 4     
2default:defaulth p
x
� 
<
%s
*synth2$
Module aes_top 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      4 Bit       Adders := 1     
2default:defaulth p
x
� 
8
%s
*synth2 
+---XORs : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input    128 Bit         XORs := 5     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	              128 Bit    Registers := 16    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                4 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 2     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input    128 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input    128 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   3 Input    128 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      4 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      4 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   3 Input      2 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 17    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      1 Bit        Muxes := 15    
2default:defaulth p
x
� 
O
%s
*synth27
#Module AesCryptoCore_v1_0_S00_AXI 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               32 Bit    Registers := 16    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                6 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                2 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 6     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     32 Bit        Muxes := 31    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	  20 Input     32 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      8 Bit        Muxes := 75    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 6     
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
[
%s
*synth2C
/Finished RTL Hierarchical Component Statistics
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
H
%s
*synth20
Start Part Resource Summary
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s
*synth2k
WPart Resources:
DSPs: 220 (col length:60)
BRAMs: 280 (col length: RAMB18 60 RAMB36 30)
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
K
%s
*synth23
Finished Part Resource Summary
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
W
%s
*synth2?
+Start Cross Boundary and Area Optimization
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
]
%s
*synth2E
1Warning: Parallel synthesis criteria is not met 
2default:defaulth p
x
� 
�
^ROM "%s" won't be mapped to RAM because address size (%s) is larger than maximum supported(%s)3997*oasys2)
w_extended_key_reg[1]2default:default2
322default:default2
252default:defaultZ8-5545h px� 
�
^ROM "%s" won't be mapped to RAM because address size (%s) is larger than maximum supported(%s)3997*oasys2)
w_extended_key_reg[2]2default:default2
322default:default2
252default:defaultZ8-5545h px� 
�
^ROM "%s" won't be mapped to RAM because address size (%s) is larger than maximum supported(%s)3997*oasys2)
w_extended_key_reg[3]2default:default2
322default:default2
252default:defaultZ8-5545h px� 
�
^ROM "%s" won't be mapped to RAM because address size (%s) is larger than maximum supported(%s)3997*oasys2)
w_extended_key_reg[4]2default:default2
322default:default2
252default:defaultZ8-5545h px� 
�
^ROM "%s" won't be mapped to RAM because address size (%s) is larger than maximum supported(%s)3997*oasys2)
w_extended_key_reg[5]2default:default2
322default:default2
252default:defaultZ8-5545h px� 
�
^ROM "%s" won't be mapped to RAM because address size (%s) is larger than maximum supported(%s)3997*oasys2)
w_extended_key_reg[6]2default:default2
322default:default2
252default:defaultZ8-5545h px� 
�
^ROM "%s" won't be mapped to RAM because address size (%s) is larger than maximum supported(%s)3997*oasys2)
w_extended_key_reg[7]2default:default2
322default:default2
252default:defaultZ8-5545h px� 
�
^ROM "%s" won't be mapped to RAM because address size (%s) is larger than maximum supported(%s)3997*oasys2)
w_extended_key_reg[8]2default:default2
322default:default2
252default:defaultZ8-5545h px� 
�
^ROM "%s" won't be mapped to RAM because address size (%s) is larger than maximum supported(%s)3997*oasys2)
w_extended_key_reg[9]2default:default2
322default:default2
252default:defaultZ8-5545h px� 
�
^ROM "%s" won't be mapped to RAM because address size (%s) is larger than maximum supported(%s)3997*oasys2*
w_extended_key_reg[10]2default:default2
322default:default2
252default:defaultZ8-5545h px� 
�
^ROM "%s" won't be mapped to RAM because address size (%s) is larger than maximum supported(%s)3997*oasys2*
w_extended_key_reg[11]2default:default2
322default:default2
252default:defaultZ8-5545h px� 
�
[ROM "%s" won't be mapped to Block RAM because address size (%s) smaller than threshold (%s)3996*oasys2D
0AesCryptoCore_v1_0_S00_AXI_inst/v2_memory_reg[0]2default:default2
42default:default2
52default:defaultZ8-5544h px� 
�
[ROM "%s" won't be mapped to Block RAM because address size (%s) smaller than threshold (%s)3996*oasys2D
0AesCryptoCore_v1_0_S00_AXI_inst/v2_memory_reg[1]2default:default2
42default:default2
52default:defaultZ8-5544h px� 
�
[ROM "%s" won't be mapped to Block RAM because address size (%s) smaller than threshold (%s)3996*oasys2D
0AesCryptoCore_v1_0_S00_AXI_inst/v2_memory_reg[2]2default:default2
42default:default2
52default:defaultZ8-5544h px� 
�
[ROM "%s" won't be mapped to Block RAM because address size (%s) smaller than threshold (%s)3996*oasys2D
0AesCryptoCore_v1_0_S00_AXI_inst/v2_memory_reg[3]2default:default2
42default:default2
52default:defaultZ8-5544h px� 
�
[ROM "%s" won't be mapped to Block RAM because address size (%s) smaller than threshold (%s)3996*oasys2D
0AesCryptoCore_v1_0_S00_AXI_inst/v2_memory_reg[4]2default:default2
42default:default2
52default:defaultZ8-5544h px� 
�
[ROM "%s" won't be mapped to Block RAM because address size (%s) smaller than threshold (%s)3996*oasys2D
0AesCryptoCore_v1_0_S00_AXI_inst/v2_memory_reg[5]2default:default2
42default:default2
52default:defaultZ8-5544h px� 
�
[ROM "%s" won't be mapped to Block RAM because address size (%s) smaller than threshold (%s)3996*oasys2D
0AesCryptoCore_v1_0_S00_AXI_inst/v2_memory_reg[6]2default:default2
42default:default2
52default:defaultZ8-5544h px� 
�
[ROM "%s" won't be mapped to Block RAM because address size (%s) smaller than threshold (%s)3996*oasys2D
0AesCryptoCore_v1_0_S00_AXI_inst/v2_memory_reg[7]2default:default2
42default:default2
52default:defaultZ8-5544h px� 
�
[ROM "%s" won't be mapped to Block RAM because address size (%s) smaller than threshold (%s)3996*oasys2D
0AesCryptoCore_v1_0_S00_AXI_inst/v2_memory_reg[8]2default:default2
42default:default2
52default:defaultZ8-5544h px� 
�
[ROM "%s" won't be mapped to Block RAM because address size (%s) smaller than threshold (%s)3996*oasys2D
0AesCryptoCore_v1_0_S00_AXI_inst/v2_memory_reg[9]2default:default2
42default:default2
52default:defaultZ8-5544h px� 
�
[ROM "%s" won't be mapped to Block RAM because address size (%s) smaller than threshold (%s)3996*oasys2E
1AesCryptoCore_v1_0_S00_AXI_inst/v2_memory_reg[10]2default:default2
42default:default2
52default:defaultZ8-5544h px� 
�
[ROM "%s" won't be mapped to Block RAM because address size (%s) smaller than threshold (%s)3996*oasys2E
1AesCryptoCore_v1_0_S00_AXI_inst/v2_memory_reg[11]2default:default2
42default:default2
52default:defaultZ8-5544h px� 
�
[ROM "%s" won't be mapped to Block RAM because address size (%s) smaller than threshold (%s)3996*oasys2E
1AesCryptoCore_v1_0_S00_AXI_inst/v2_memory_reg[12]2default:default2
42default:default2
52default:defaultZ8-5544h px� 
�
[ROM "%s" won't be mapped to Block RAM because address size (%s) smaller than threshold (%s)3996*oasys2E
1AesCryptoCore_v1_0_S00_AXI_inst/v2_memory_reg[13]2default:default2
42default:default2
52default:defaultZ8-5544h px� 
�
[ROM "%s" won't be mapped to Block RAM because address size (%s) smaller than threshold (%s)3996*oasys2E
1AesCryptoCore_v1_0_S00_AXI_inst/v2_memory_reg[14]2default:default2
42default:default2
52default:defaultZ8-5544h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2U
A\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][88] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2U
A\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][89] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2U
A\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][90] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2U
A\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][91] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2U
A\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][92] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2U
A\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][93] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2U
A\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][94] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2U
A\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][95] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2U
A\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][80] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2U
A\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][81] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2U
A\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][82] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2U
A\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][83] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2U
A\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][84] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2U
A\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][85] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2U
A\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][86] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2U
A\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][87] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2U
A\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][72] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2U
A\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][73] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2U
A\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][74] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2U
A\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][75] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2U
A\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][76] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2U
A\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][77] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2U
A\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][78] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2U
A\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][79] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2U
A\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][64] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2U
A\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][65] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2U
A\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][66] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2U
A\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][67] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2U
A\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][68] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2U
A\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][69] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2U
A\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][70] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2U
A\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][71] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2U
A\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][24] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2U
A\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][25] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2U
A\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][26] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2U
A\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][27] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2U
A\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][28] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2U
A\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][29] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2U
A\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][30] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2U
A\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][31] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2U
A\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][16] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2U
A\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][17] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2U
A\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][18] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2U
A\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][19] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2U
A\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][20] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2U
A\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][21] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2U
A\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][22] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2U
A\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][23] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2T
@\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][8] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2T
@\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][9] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2U
A\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][10] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2U
A\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][11] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2U
A\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][12] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2U
A\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][13] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2U
A\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][14] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2U
A\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][15] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2T
@\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][0] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2T
@\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][1] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2T
@\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][2] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2T
@\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][3] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2T
@\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][4] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2T
@\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][5] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2T
@\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][6] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2T
@\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][7] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2V
B\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][120] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2V
B\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][121] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2V
B\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][122] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2V
B\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][123] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2V
B\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][124] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2V
B\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][125] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2V
B\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][126] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2V
B\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][127] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2V
B\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][112] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2V
B\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][113] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2V
B\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][114] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2V
B\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][115] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2V
B\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][116] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2V
B\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][117] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2V
B\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][118] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2V
B\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][119] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2V
B\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][104] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2V
B\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][105] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2V
B\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][106] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2V
B\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][107] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2V
B\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][108] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2V
B\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][109] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2V
B\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][110] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2V
B\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][111] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2U
A\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][96] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2U
A\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][97] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2U
A\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][98] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2U
A\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][99] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2V
B\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][100] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2V
B\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][101] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2V
B\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][102] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2V
B\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][103] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2U
A\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][56] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2U
A\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][57] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2U
A\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][58] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2U
A\AesCryptoCore_v1_0_S00_AXI_inst/UIP /\w_extended_key_reg[0][59] 2default:defaultZ8-3333h px� 
�
�Message '%s' appears more than %s times and has been disabled. User can change this message limit to see more message instances.
14*common2 
Synth 8-33332default:default2
1002default:defaultZ17-14h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2D
0AesCryptoCore_v1_0_S00_AXI_inst/axi_rresp_reg[0]2default:default2
FDRE2default:default2D
0AesCryptoCore_v1_0_S00_AXI_inst/axi_rresp_reg[1]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2D
0AesCryptoCore_v1_0_S00_AXI_inst/axi_bresp_reg[0]2default:default2
FDRE2default:default2D
0AesCryptoCore_v1_0_S00_AXI_inst/axi_bresp_reg[1]2default:defaultZ8-3886h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2.
w_extended_key_reg[0][127]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2.
w_extended_key_reg[0][126]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2.
w_extended_key_reg[0][125]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2.
w_extended_key_reg[0][124]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2.
w_extended_key_reg[0][123]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2.
w_extended_key_reg[0][122]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2.
w_extended_key_reg[0][121]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2.
w_extended_key_reg[0][120]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2.
w_extended_key_reg[0][119]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2.
w_extended_key_reg[0][118]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2.
w_extended_key_reg[0][117]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2.
w_extended_key_reg[0][116]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2.
w_extended_key_reg[0][115]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2.
w_extended_key_reg[0][114]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2.
w_extended_key_reg[0][113]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2.
w_extended_key_reg[0][112]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2.
w_extended_key_reg[0][111]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2.
w_extended_key_reg[0][110]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2.
w_extended_key_reg[0][109]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2.
w_extended_key_reg[0][108]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2.
w_extended_key_reg[0][107]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2.
w_extended_key_reg[0][106]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2.
w_extended_key_reg[0][105]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2.
w_extended_key_reg[0][104]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2.
w_extended_key_reg[0][103]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2.
w_extended_key_reg[0][102]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2.
w_extended_key_reg[0][101]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2.
w_extended_key_reg[0][100]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2-
w_extended_key_reg[0][99]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2-
w_extended_key_reg[0][98]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2-
w_extended_key_reg[0][97]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2-
w_extended_key_reg[0][96]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2-
w_extended_key_reg[0][95]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2-
w_extended_key_reg[0][94]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2-
w_extended_key_reg[0][93]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2-
w_extended_key_reg[0][92]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2-
w_extended_key_reg[0][91]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2-
w_extended_key_reg[0][90]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2-
w_extended_key_reg[0][89]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2-
w_extended_key_reg[0][88]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2-
w_extended_key_reg[0][87]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2-
w_extended_key_reg[0][86]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2-
w_extended_key_reg[0][85]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2-
w_extended_key_reg[0][84]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2-
w_extended_key_reg[0][83]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2-
w_extended_key_reg[0][82]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2-
w_extended_key_reg[0][81]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2-
w_extended_key_reg[0][80]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2-
w_extended_key_reg[0][79]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2-
w_extended_key_reg[0][78]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2-
w_extended_key_reg[0][77]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2-
w_extended_key_reg[0][76]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2-
w_extended_key_reg[0][75]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2-
w_extended_key_reg[0][74]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2-
w_extended_key_reg[0][73]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2-
w_extended_key_reg[0][72]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2-
w_extended_key_reg[0][71]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2-
w_extended_key_reg[0][70]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2-
w_extended_key_reg[0][69]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2-
w_extended_key_reg[0][68]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2-
w_extended_key_reg[0][67]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2-
w_extended_key_reg[0][66]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2-
w_extended_key_reg[0][65]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2-
w_extended_key_reg[0][64]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2-
w_extended_key_reg[0][63]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2-
w_extended_key_reg[0][62]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2-
w_extended_key_reg[0][61]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2-
w_extended_key_reg[0][60]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2-
w_extended_key_reg[0][59]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2-
w_extended_key_reg[0][58]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2-
w_extended_key_reg[0][57]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2-
w_extended_key_reg[0][56]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2-
w_extended_key_reg[0][55]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2-
w_extended_key_reg[0][54]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2-
w_extended_key_reg[0][53]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2-
w_extended_key_reg[0][52]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2-
w_extended_key_reg[0][51]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2-
w_extended_key_reg[0][50]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2-
w_extended_key_reg[0][49]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2-
w_extended_key_reg[0][48]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2-
w_extended_key_reg[0][47]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2-
w_extended_key_reg[0][46]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2-
w_extended_key_reg[0][45]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2-
w_extended_key_reg[0][44]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2-
w_extended_key_reg[0][43]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2-
w_extended_key_reg[0][42]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2-
w_extended_key_reg[0][41]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2-
w_extended_key_reg[0][40]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2-
w_extended_key_reg[0][39]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2-
w_extended_key_reg[0][38]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2-
w_extended_key_reg[0][37]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2-
w_extended_key_reg[0][36]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2-
w_extended_key_reg[0][35]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2-
w_extended_key_reg[0][34]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2-
w_extended_key_reg[0][33]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2-
w_extended_key_reg[0][32]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2-
w_extended_key_reg[0][31]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2-
w_extended_key_reg[0][30]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2-
w_extended_key_reg[0][29]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2-
w_extended_key_reg[0][28]2default:default2
aes_top2default:defaultZ8-3332h px� 
�
�Message '%s' appears more than %s times and has been disabled. User can change this message limit to see more message instances.
14*common2 
Synth 8-33322default:default2
1002default:defaultZ17-14h px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Cross Boundary and Area Optimization : Time (s): cpu = 00:01:18 ; elapsed = 00:01:29 . Memory (MB): peak = 849.234 ; gain = 519.277
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
E
%s
*synth2-

Report RTL Partitions: 
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
+| |RTL Partition |Replication |Instances |
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
F
%s
*synth2.
Start Timing Optimization
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
|Finished Timing Optimization : Time (s): cpu = 00:01:24 ; elapsed = 00:01:36 . Memory (MB): peak = 849.234 ; gain = 519.277
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
E
%s
*synth2-

Report RTL Partitions: 
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
+| |RTL Partition |Replication |Instances |
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
E
%s
*synth2-
Start Technology Mapping
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
{Finished Technology Mapping : Time (s): cpu = 00:01:31 ; elapsed = 00:01:44 . Memory (MB): peak = 849.234 ; gain = 519.277
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
E
%s
*synth2-

Report RTL Partitions: 
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
+| |RTL Partition |Replication |Instances |
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
?
%s
*synth2'
Start IO Insertion
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
Q
%s
*synth29
%Start Flattening Before IO Insertion
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
T
%s
*synth2<
(Finished Flattening Before IO Insertion
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
H
%s
*synth20
Start Final Netlist Cleanup
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
K
%s
*synth23
Finished Final Netlist Cleanup
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
uFinished IO Insertion : Time (s): cpu = 00:01:35 ; elapsed = 00:01:47 . Memory (MB): peak = 849.234 ; gain = 519.277
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
D
%s
*synth2,

Report Check Netlist: 
2default:defaulth p
x
� 
u
%s
*synth2]
I+------+------------------+-------+---------+-------+------------------+
2default:defaulth p
x
� 
u
%s
*synth2]
I|      |Item              |Errors |Warnings |Status |Description       |
2default:defaulth p
x
� 
u
%s
*synth2]
I+------+------------------+-------+---------+-------+------------------+
2default:defaulth p
x
� 
u
%s
*synth2]
I|1     |multi_driven_nets |      0|        0|Passed |Multi driven nets |
2default:defaulth p
x
� 
u
%s
*synth2]
I+------+------------------+-------+---------+-------+------------------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
O
%s
*synth27
#Start Renaming Generated Instances
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Renaming Generated Instances : Time (s): cpu = 00:01:35 ; elapsed = 00:01:47 . Memory (MB): peak = 849.234 ; gain = 519.277
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
E
%s
*synth2-

Report RTL Partitions: 
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
+| |RTL Partition |Replication |Instances |
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
L
%s
*synth24
 Start Rebuilding User Hierarchy
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Rebuilding User Hierarchy : Time (s): cpu = 00:01:35 ; elapsed = 00:01:48 . Memory (MB): peak = 849.234 ; gain = 519.277
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
K
%s
*synth23
Start Renaming Generated Ports
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Renaming Generated Ports : Time (s): cpu = 00:01:35 ; elapsed = 00:01:48 . Memory (MB): peak = 849.234 ; gain = 519.277
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
M
%s
*synth25
!Start Handling Custom Attributes
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Handling Custom Attributes : Time (s): cpu = 00:01:35 ; elapsed = 00:01:48 . Memory (MB): peak = 849.234 ; gain = 519.277
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
J
%s
*synth22
Start Renaming Generated Nets
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Renaming Generated Nets : Time (s): cpu = 00:01:36 ; elapsed = 00:01:48 . Memory (MB): peak = 849.234 ; gain = 519.277
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
K
%s
*synth23
Start Writing Synthesis Report
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
A
%s
*synth2)

Report BlackBoxes: 
2default:defaulth p
x
� 
J
%s
*synth22
+-+--------------+----------+
2default:defaulth p
x
� 
J
%s
*synth22
| |BlackBox name |Instances |
2default:defaulth p
x
� 
J
%s
*synth22
+-+--------------+----------+
2default:defaulth p
x
� 
J
%s
*synth22
+-+--------------+----------+
2default:defaulth p
x
� 
A
%s*synth2)

Report Cell Usage: 
2default:defaulth px� 
D
%s*synth2,
+------+-------+------+
2default:defaulth px� 
D
%s*synth2,
|      |Cell   |Count |
2default:defaulth px� 
D
%s*synth2,
+------+-------+------+
2default:defaulth px� 
D
%s*synth2,
|1     |BUFG   |     1|
2default:defaulth px� 
D
%s*synth2,
|2     |CARRY4 |    11|
2default:defaulth px� 
D
%s*synth2,
|3     |LUT1   |     2|
2default:defaulth px� 
D
%s*synth2,
|4     |LUT2   |   286|
2default:defaulth px� 
D
%s*synth2,
|5     |LUT3   |   248|
2default:defaulth px� 
D
%s*synth2,
|6     |LUT4   |   494|
2default:defaulth px� 
D
%s*synth2,
|7     |LUT5   |   536|
2default:defaulth px� 
D
%s*synth2,
|8     |LUT6   |  1665|
2default:defaulth px� 
D
%s*synth2,
|9     |MUXF7  |   192|
2default:defaulth px� 
D
%s*synth2,
|10    |MUXF8  |    32|
2default:defaulth px� 
D
%s*synth2,
|11    |FDRE   |  2304|
2default:defaulth px� 
D
%s*synth2,
|12    |FDSE   |     3|
2default:defaulth px� 
D
%s*synth2,
|13    |IBUF   |    51|
2default:defaulth px� 
D
%s*synth2,
|14    |OBUF   |    41|
2default:defaulth px� 
D
%s*synth2,
+------+-------+------+
2default:defaulth px� 
E
%s
*synth2-

Report Instance Areas: 
2default:defaulth p
x
� 
{
%s
*synth2c
O+------+----------------------------------+---------------------------+------+
2default:defaulth p
x
� 
{
%s
*synth2c
O|      |Instance                          |Module                     |Cells |
2default:defaulth p
x
� 
{
%s
*synth2c
O+------+----------------------------------+---------------------------+------+
2default:defaulth p
x
� 
{
%s
*synth2c
O|1     |top                               |                           |  5866|
2default:defaulth p
x
� 
{
%s
*synth2c
O|2     |  AesCryptoCore_v1_0_S00_AXI_inst |AesCryptoCore_v1_0_S00_AXI |  5773|
2default:defaulth p
x
� 
{
%s
*synth2c
O|3     |    UIP                           |aes_top                    |  5349|
2default:defaulth p
x
� 
{
%s
*synth2c
O|4     |      mix_inst0                   |MixColumns                 |    81|
2default:defaulth p
x
� 
{
%s
*synth2c
O|5     |      mix_inst1                   |MixColumns_0               |    81|
2default:defaulth p
x
� 
{
%s
*synth2c
O|6     |      mix_inst2                   |MixColumns_1               |    81|
2default:defaulth p
x
� 
{
%s
*synth2c
O|7     |      mix_inst3                   |MixColumns_2               |    81|
2default:defaulth p
x
� 
{
%s
*synth2c
O+------+----------------------------------+---------------------------+------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Writing Synthesis Report : Time (s): cpu = 00:01:36 ; elapsed = 00:01:48 . Memory (MB): peak = 849.234 ; gain = 519.277
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
t
%s
*synth2\
HSynthesis finished with 0 errors, 0 critical warnings and 538 warnings.
2default:defaulth p
x
� 
�
%s
*synth2�
~Synthesis Optimization Runtime : Time (s): cpu = 00:01:36 ; elapsed = 00:01:48 . Memory (MB): peak = 849.234 ; gain = 519.277
2default:defaulth p
x
� 
�
%s
*synth2�
Synthesis Optimization Complete : Time (s): cpu = 00:01:36 ; elapsed = 00:01:48 . Memory (MB): peak = 849.234 ; gain = 519.277
2default:defaulth p
x
� 
B
 Translating synthesized netlist
350*projectZ1-571h px� 
g
-Analyzing %s Unisim elements for replacement
17*netlist2
2862default:defaultZ29-17h px� 
j
2Unisim Transformation completed in %s CPU seconds
28*netlist2
02default:defaultZ29-28h px� 
�
�Netlist '%s' is not ideal for floorplanning, since the cellview '%s' contains a large number of primitives.  Please consider enabling hierarchy in synthesis if you want to do floorplanning.
310*netlist2&
AesCryptoCore_v1_02default:default2
aes_top2default:defaultZ29-101h px� 
K
)Preparing netlist for logic optimization
349*projectZ1-570h px� 
u
)Pushed %s inverter(s) to %s load pin(s).
98*opt2
02default:default2
02default:defaultZ31-138h px� 
~
!Unisim Transformation Summary:
%s111*project29
%No Unisim elements were transformed.
2default:defaultZ1-111h px� 
U
Releasing license: %s
83*common2
	Synthesis2default:defaultZ17-83h px� 
�
G%s Infos, %s Warnings, %s Critical Warnings and %s Errors encountered.
28*	vivadotcl2
2122default:default2
2012default:default2
02default:default2
02default:defaultZ4-41h px� 
^
%s completed successfully
29*	vivadotcl2 
synth_design2default:defaultZ4-42h px� 
�
I%sTime (s): cpu = %s ; elapsed = %s . Memory (MB): peak = %s ; gain = %s
268*common2"
synth_design: 2default:default2
00:01:472default:default2
00:02:052default:default2
849.2342default:default2
531.5862default:defaultZ17-268h px� 
K
"No constraint will be written out.1103*constraintsZ18-5210h px� 
�
 The %s '%s' has been generated.
621*common2

checkpoint2default:default2�
zd:/digiLent_test/final/final.tmp/aescryptocore_v1_0_project/AesCryptoCore_v1_0_project.runs/synth_1/AesCryptoCore_v1_0.dcp2default:defaultZ17-1381h px� 
�
%s4*runtcl2�
zExecuting : report_utilization -file AesCryptoCore_v1_0_utilization_synth.rpt -pb AesCryptoCore_v1_0_utilization_synth.pb
2default:defaulth px� 
�
sreport_utilization: Time (s): cpu = 00:00:01 ; elapsed = 00:00:00.248 . Memory (MB): peak = 849.234 ; gain = 0.000
*commonh px� 
�
Exiting %s at %s...
206*common2
Vivado2default:default2,
Thu Jun 20 10:31:02 20192default:defaultZ17-206h px� 


End Record